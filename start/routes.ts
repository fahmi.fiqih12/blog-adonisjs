/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

// Route.get('/', async () => {
//   return { hello: 'world' }
// })

  Route.post('/api/register', 'UsersController.register').as('auth.register')
  Route.post('/login', 'UsersController.login').as('auth.login')
  Route.post('/confirmation-otp', 'UsersController.otpConfirmastion').as('auth.otpConfirmastion')
  
  Route.post('/article', 'ArticlesController.store').as('auth.store').middleware(['auth','verify'])
  Route.get('/article', 'ArticlesController.index').as('auth.index').middleware(['auth','verify'])
  
  Route.post('/profile', 'ProfilesController.store').as('profile.store').middleware('auth')
  Route.get('/api/hello', 'TestsController.hello')

  // Route
  // .group(() => {
  //   Route.get('/users', 'UsersController.index')
  //   Route.get('/posts', 'PostsController.index')
  // })
  // .prefix('/api')


