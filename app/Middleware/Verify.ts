import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Verify {
  public async handle ({ auth, response}: HttpContextContract, next: () => Promise<void>) {

      let isVerified = auth.user?.isVerified
      if(isVerified){
        await next()
      }else{
        return response.unauthorized({
          message: 'your token is not verified'
        })
      }

      
  }
}
