import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Profile from 'App/Models/Profile'

export default class ProfilesController {
  public async index ({}: HttpContextContract) {
  }

  public async store ({ request, response, auth }: HttpContextContract) {
    const fullName = request.input('fullName')
    const phone = request.input('phone')

    // Profile.create({
    //   fullName: fullName,
    //   phone: phone,
    //   userId: auth.user?.id,
    // })

    const authUser = auth.user
    authUser?.related('profile').create({
      fullName: fullName,
      phone: phone
    })

    return response.status(201).json({
      message: 'success craetd'
    })
  }

  public async show ({}: HttpContextContract) {
  }

  public async update ({}: HttpContextContract) {
  }

  public async destroy ({}: HttpContextContract) {
  }
}
