import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import UserValidator from 'App/Validators/UserValidator'
import Mail from '@ioc:Adonis/Addons/Mail'
import User from 'App/Models/User'
import Database from '@ioc:Adonis/Lucid/Database'

export default class UsersController {

  /**
  * @swagger
  * /api/register:
  *   post:
  *     tags:
  *       - Authentication  
  *     requestBody:    
  *       required: true  
  *       content:    
  *        application/x-www-form-urlencoded:
  *          schema:
  *            $ref: '#definitions/User'
  *     responses:
  *       '201':
  *         description: user created, verify opt in eamil
  *       '422': 
  *         description: request invalid
  */
    public async register({ request, response }: HttpContextContract){
        try {   
            await request.validate(UserValidator)

            const name = request.input('name')
            const email = request.input('email')
            const password = request.input('password')

            const user =  await User.create({ name, email, password })
            const otpCode = Math.floor(100000 + Math.random() * 900000)
            await Database.table('otp_codes').insert({ 
                otp_code: otpCode,
                user_id: user.id
            })
            await Mail.send((message) => {
                message
                  .from('admin@example.com')
                  .to(email)
                  .subject('Welcome in Adonisjs!')
                  .htmlView('email/otp_verification', { otpCode })
              })
            
            response.status(200).json({
                message: 'Register success, please verify your otp!'
            })
        } catch (error) {
            return response.unprocessableEntity({
                message: error.messages
            })
        }
    }

    public async login({ request, response, auth}: HttpContextContract){
        const UsersSchema = schema.create({
            email: schema.string(),
            password: schema.string()
        })

        const email = request.input('email')
        const password = request.input('password')

        try {
            await request.validate({ schema: UsersSchema })
            const token = await auth.use('api').attempt(email, password)
            return response.status(200).json({
                message: 'Login success',
                token
            })
        } catch (error) {
            if(error.guard){
                return response.badRequest({
                    message: 'Login error',
                    error: error.message
                })
            }else{
                return response.badRequest({
                    message: 'Login error',
                    error: error.messages
                })
            }
          
        }
    }

    public async otpConfirmastion({ request, response}: HttpContextContract){
        let otp_code = request.input('otp_code')
        let email = request.input('email')

        let user = await User.findBy('email', email)
        let otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).first()
        console.log("ini id dari otp",otpCheck.user_id)
        if(user?.id == otpCheck.user_id){
            user.isVerified = true
            await user?.save()
            return response.status(200).json({
                status: 200,
                message: 'succes confirmation OTP'
            })
        }
    }
}
