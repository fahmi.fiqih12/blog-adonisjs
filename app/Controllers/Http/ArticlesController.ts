import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import ArticleValidator from 'App/Validators/ArticleValidator'
import Articel from 'App/Models/Articel'
import Tag from 'App/Models/Tag'
interface TagInterface {
  name: string
}

export default class ArticlesController {
  public async index ({ response  }: HttpContextContract) {
      // const article = await Articel.all()
      
      const article = await Articel.query().preload('author')
      return response.status(200).json({
        status: 'ok',
        message: 'managed to get data',
        data: article
      })
  }

  public async store ({ request, response, auth }: HttpContextContract) {
    try {
      const data = await request.validate(ArticleValidator)
        // const aritcle = new Articel()
        // aritcle.title = request.input('title')
        // aritcle.body = request.input('body')
        // await aritcle.save()
        // await Articel.create(data)

        const newArt = new Articel()
        newArt.title = data.title
        newArt.body = data.body

        let arrTags: string[] = request.input('tags').split(',')
        let tags: TagInterface[] = arrTags.map(el => { return { name: el } })
        
        let newTags = await Tag.fetchOrCreateMany('name', tags)
        let tagIds: number[] = newTags.map(tag => tag.id) 
      
        const authUser = auth.user

        await authUser?.related('articel').save(newArt)

        await newArt.related('tags').sync(tagIds)

        return response.created({
          message: 'success created'
        })
    } catch (error) {
      return response.unprocessableEntity({
        message: error.messages
      })
    }
  }


  public async show ({}: HttpContextContract) {
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({}: HttpContextContract) {
  }

  public async destroy ({}: HttpContextContract) {
  }
}
