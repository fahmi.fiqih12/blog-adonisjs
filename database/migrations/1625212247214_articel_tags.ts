import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ArticelTags extends BaseSchema {
  protected tableName = 'articel_tag'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('tag_id').unsigned().references('tags.id')
      table.integer('articel_id').unsigned().references('articels.id')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
