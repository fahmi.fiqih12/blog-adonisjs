import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Articels extends BaseSchema {
  protected tableName = 'articels'

  public async up () {
    this.schema.table(this.tableName, (table) => {
      table.integer('user_id').unsigned().references('users.id').onDelete('CASCADE')
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropForeign('user_id')
      table.dropColumn('user_id')
    })
  }
}
